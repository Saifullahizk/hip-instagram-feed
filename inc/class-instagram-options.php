<?php

class Hip_Instagram_Feed_Options{
	/**
	 * Hold the class instance.
	 */
	private static $instance = null;

	/**
	 * Holds the default values
	 */

	private $defaults = array(
		'access_token' => '',
	);

	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Json field data
	 */
	private $json_fields;

	/**
	 * Settings key in database, used in get_option() as first parameter
	 *
	 * @var string
	 */
	private $settings_key = 'hip_instagram_feed_settings';

	/**
	 * Slug of the page, also used as identifier for hooks
	 *
	 * @var string
	 */
	private $slug = 'hip-instagram-feed';

	/**
	 * Options group id, will be used as identifier for adding fields to options page
	 *
	 * @var string
	 */
	private $options_group_id = 'hip-instagram-feed-group-id';

	/**
	 * Fields var
	 *
	 * @var string
	 */
	private $fields;

	/**
	 * Field data var
	 *
	 * @var string
	 */
	private $database_options;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->init();
	}
	/**
	 * Get Class Instance
	 */
	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new Hip_Instagram_Feed_Options();
		}

		return self::$instance;
	}

	/**
	 * Initialize method
	 */
	public function init() {

		$this->database_options = get_option( $this->settings_key );

		$this->options = wp_parse_args( $this->database_options, $this->defaults );

		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
		add_filter( 'plugin_action_links', array( $this, 'plugin_settings_link' ), 10, 2 );
		$this->run_schedule();


	}

	/**Refresh tocken function
	 */

	public function refresh_token(){
		$options = get_option( 'hip_instagram_feed_settings', array() );
		$access_token= $options['access_token'];

		if ( $access_token ) {
			$apiUrl = "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=$access_token";
			$response = wp_remote_get($apiUrl);
			$responseBody = wp_remote_retrieve_body( $response );
			$result = json_decode( $responseBody, true);
			if ( is_array( $result ) && ! is_wp_error( $result ) ) {

				if (!empty($result['access_token'])) {
					$options = get_option( 'hip_instagram_feed_settings', array() );
					$options['access_token'] = $result['access_token'];
					update_option( 'hip_instagram_feed_settings', $options );
					set_transient( 'ig_refresh_token', true, 1 * DAY_IN_SECONDS );
				}
			} else {
				// Work with the error
			}
		}
	}

	/**
	 * cron job
	 */

	public function run_schedule(){
		if ( ! get_transient( 'ig_refresh_token' ) ) {
			$this->refresh_token();
		}
	}

	/* Add the plugin settings link */
	function plugin_settings_link( $actions, $file ) {

		if ( $file != HIP_INSTAGRAM_FEED_BASENAME ) {
			return $actions;
		}

		$actions['hip_instagram_feed_settings'] = '<a href="' . esc_url( admin_url( 'options-general.php?page=' . $this->slug ) ) . '" aria-label="settings"> ' . __( 'Settings', 'hip' ) . '</a>';

		return $actions;
	}

	/* Get fields data */
	function get_fields() {

		$fields = array(
			'access_token' => array(
				'id'	   => 'access_token',
				'title'	   => esc_html__( 'Access Token', 'hip' ),
				'sanitize' => 'text',
				'default'  => '',
			)
		);

		$fields = apply_filters( 'hip_instagram_feed_modify_options_fields', $fields );

		return $fields;
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page() {
		// This page will be under "Settings"
		add_options_page(
			esc_html__( 'Hip Instagram Settings', 'hip' ),
			esc_html__( 'Hip Instagram Settings', 'hip' ),
			'manage_options',
			$this->slug,
			array( $this, 'print_settings_page' )
		);
	}
	/**
	 * Options page callback
	 */
	public function print_settings_page() {
		?>
		<div class="wrap">
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<form method="post" action="options.php">
				<?php
				settings_fields( $this->options_group_id );
				do_settings_sections( $this->slug );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init() {

		register_setting(
			$this->options_group_id, // Option group
			$this->settings_key, // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		$this->fields = $this->get_fields();

		if ( empty( $this->fields ) ) {
			return false;
		}

		$section_id = 'hip_instagram_feed_section';

		add_settings_section( $section_id, '', '', $this->slug );

		foreach ( $this->fields as $field ) {

			if ( empty( $field['id'] ) ) {
				continue;
			}

			$action	  = 'print_' . $field['id'] . '_field';
			$callback = method_exists( $this, $action ) ? array( $this, $action ) : $field['action'];

			add_settings_field(
				'hip_instagram_feed_' . $field['id'] . '_id',
				$field['title'],
				$callback,
				$this->slug,
				$section_id,
				$this->options[ $field['id'] ]
			);
		}
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param unknown $input array $input Contains all settings fields as array keys
	 * @return mixed
	 */
	public function sanitize( $input ) {

		if ( empty( $this->fields ) || empty( $input ) ) {
			return false;
		}

		$new_input = array();
		foreach ( $this->fields as $field ) {
			if ( isset( $input[ $field['id'] ] ) ) {
				$new_input[ $field['id'] ] = $this->sanitize_field( $input[ $field['id'] ], $field['sanitize'] );
			}
		}

		return $new_input;
	}
	/**
	 * Dynamically sanitize field values
	 *
	 * @param unknown $value
	 * @param unknown $sensitization_type
	 * @return int|string
	 */
	private function sanitize_field( $value, $sensitization_type ) {
		switch ( $sensitization_type ) {

			case 'checkbox':
				$new_input = array();
				foreach ( $value as $key => $val ) {
					$new_input[ $key ] = ( isset( $value[ $key ] ) ) ?
						sanitize_text_field( $val ) :
						'';
				}
				return $new_input;
				break;

			case 'radio':
				return sanitize_text_field( $value );
				break;

			case 'text':
				return sanitize_text_field( $value );
				break;

			default:
				return $value;
				break;
		}
	}
    /**
     * Print button field
     */
    public function instagram_auth_button() {

        $oauth_personal_url= ' https://api.instagram.com/oauth/authorize?client_id=870524217060414&redirect_uri=https://instagram.hip.agency/auth/&scope=user_profile,user_media&response_type=code&state='. esc_url( admin_url( 'options-general.php?page=hip-instagram-feed' ) );

        ?>

        <a 	class="button button-primary instagram-connect-btn"
              data-personal-api="<?php echo esc_url( $oauth_personal_url ); ?>"
              href="<?php echo esc_url( $oauth_personal_url ); ?>">
            <span><?php echo esc_html( 'Authorize with Instagram', 'hip' ); ?></span>
        </a>

        <?php
        $this->get_access_token();

    }

	/**
	 * set & update access token
	 */
    public function  get_access_token(){
        if (!empty($_GET['token'])) {
            $options = get_option( 'hip_instagram_feed_settings', array() );
            $options['access_token'] = $_GET['token'];
            update_option( 'hip_instagram_feed_settings', $options );

        }
    }




	/**
	 * Print access token field
	 */
	public function print_access_token_field( $args ) {

		$token =  $_GET['token'] ? $_GET['token'] : $args;

		printf(
			'<label class="hip-instagram-feed-access-token">
<input type="text" id="hip-access-token" name="%s[access_token]" value="%s"/></label>',
			$this->settings_key,
				$token
		);
        $this->instagram_auth_button();
	}
}

