<?php

class Hip_Instagram_Feed_template{
    /**
     * Access Token
     *
     * @var string
     */
    private $access_token;

    /**
     * ig fields
     * @var string
     */
    private $fields = "id,caption,media_type,media_url,permalink,thumbnail_url,timestamp,username";

    /**
     * Specifies the class name and description, instantiates the widget and includes necessary stylesheets and JavaScript.
     */
    public function __construct() {
        $hip_instagram_feed_settings = get_option( 'hip_instagram_feed_settings' );

        $this->access_token = isset($hip_instagram_feed_settings['access_token']) ? $hip_instagram_feed_settings['access_token'] : '';

        add_shortcode( 'hip_instagram_shortcode', array( $this, 'get_instagram_data' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
    }

    function frontend_scripts(){
        wp_enqueue_style( 'hip_owl_carousel', HIP_INSTAGRAM_FEED_URL . 'assets/css/owl.carousel.min.css', HIP_INSTAGRAM_FEED_VER, true );
        wp_enqueue_style( 'hip_owl_carousel_theme', HIP_INSTAGRAM_FEED_URL . 'assets/css/owl.theme.default.min.css', HIP_INSTAGRAM_FEED_VER, true );
        wp_enqueue_style( 'hip_instagram_feed', HIP_INSTAGRAM_FEED_URL . 'assets/css/instagram-feed.css', HIP_INSTAGRAM_FEED_VER, true );

        wp_enqueue_script( 'hip_owl_carousel', HIP_INSTAGRAM_FEED_URL . 'assets/js/owl.carousel.min.js', array( 'jquery' ), HIP_INSTAGRAM_FEED_VER, true );
        wp_enqueue_script( 'hip_custom_js', HIP_INSTAGRAM_FEED_URL . 'assets/js/custom.js', array( 'jquery', 'hip_owl_carousel' ), HIP_INSTAGRAM_FEED_VER, true );
    }

    /**
     * Make remote request base on Instagram endpoints, get JSON and collect slider images.
     *
     * @since 1.0.0
     *
     * @return array  - List of collected images
     */
    public function get_instagram_data($atts) {
        $options = shortcode_atts( array(
            'mockup-url' =>  HIP_INSTAGRAM_FEED_URL	.'/assets/images/iphone-mockup.png',
            'photo-limit' => 10
        ), $atts );
        return $this->get_instagram_data_with_token($options['mockup-url'], $options['photo-limit']);
    }

    /**
     * Make request with token.
     *
     * @since 1.0.0
     *
     * @return array List of collected images 17841400116381693
     */
    protected function get_instagram_data_with_token($mockup_url, $limit) {
        if(empty($this->access_token)){
            return false;
        }
        $response = wp_remote_get( "https://graph.instagram.com/me/media?fields={$this->fields}&access_token={$this->access_token}&limit={$limit}");
        $data = json_decode( wp_remote_retrieve_body( $response ),true );

        if ( is_wp_error( $response ) || 200 != wp_remote_retrieve_response_code( $response ) ) {
            $error_message = __('Connection error. Connection fail between instagram and your server. Please try again', 'hip');
            if ( isset($data->error->message) ) {
                $error_message = str_replace( '(#100)', 'Error message: ', $data->error->message );
            }
            echo '';
            error_log( print_r(new WP_Error( 'invalid-token', esc_html( $error_message ) ), true));
            return '';
        }

        echo "<div class='hip_instagram_feed_container'>";
        echo '<div class="mockup_overlay"><img class="hip_instagram_feed_frame" src="'.$mockup_url.'"/></div>';
        echo '<div class="hip_instagram_feed_inner owl-carousel owl-theme" id="hip_instagram_feed">';

        foreach($data["data"] as $post){

            $username = isset($post["username"]) ? $post["username"] : "";
            $caption = isset($post["caption"]) ? $post["caption"] : "";
            $media_url = isset($post["media_url"]) ? $post["media_url"] : "";
            $permalink = isset($post["permalink"]) ? $post["permalink"] : "";
            $media_type = isset($post["media_type"]) ? $post["media_type"] : "";

            if($media_type=="IMAGE" || $media_type == "CAROUSEL_ALBUM"){
                echo " <div class='hip_ig_post_item item'>";
                echo "<img src='{$media_url}' /> ";
                echo "</div>";
            }
        }
        echo " </div>";
        echo " </div>";


    }
}
