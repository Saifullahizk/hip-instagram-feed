# Hip Instagram Feed

For use this plugin you need to use a shortcode for show instagram feed

## Installation / Activation

Install this plugin 

```bash
composer require "hipdevteam/hip-instagram-feed":"*"
```

## Usage

```python
#Use this Shortcode where you want to show feed
[hip_instagram_shortcode]

# If you want to change the mockup Image just pass the image URL
[hip_instagram_shortcode mockup-url="/wp-content/upload/2021/01/mockup.png"]

```
