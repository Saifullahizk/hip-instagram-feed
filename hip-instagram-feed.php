<?php
/*
Plugin Name: Hip Instagram Feed
Description: This plugin for hip client by Hip Creative, Inc.
Version: 2.0.2
Plugin URI:  https://hip.agency/
Author:      Hip Creative
Author URI:  https://hip.agency/
Text Domain: hip
*/

/* Prevent Direct access */
if ( ! defined( 'ABSPATH' ) ) { exit; }

define( 'HIP_INSTAGRAM_FEED_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'HIP_INSTAGRAM_FEED_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'HIP_INSTAGRAM_FEED_VER', '2.0.2' );
define( 'HIP_INSTAGRAM_FEED_BASENAME', plugin_basename( __FILE__ ) );

/* Initialize Options */
if ( !function_exists( 'hip_instagram_feed_options_init' ) ):
	function hip_instagram_feed_options_init() {

		require_once HIP_INSTAGRAM_FEED_DIR.'inc/class-instagram-options.php';
		require_once HIP_INSTAGRAM_FEED_DIR.'inc/template-view.php';
		Hip_Instagram_Feed_Options::get_instance();
		$template_view = new Hip_Instagram_Feed_template();
	}
endif;

add_action( 'init', 'hip_instagram_feed_options_init' );
